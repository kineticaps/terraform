variable "vpc_id" {
  description = "The VPC id into which to launch the AWS instances"
}

variable "subnet_id" {
  description = "The VPC subnet into which to launch the AWS instances"
}

variable "vpc_security_group_ids" {
  description = "A list of additional security group ids to apply to each configuration node."
  default     = []
  type        = "list"
}

data "aws_subnet" "kineticadb" {
  id = "${var.subnet_id}"
}

resource "aws_security_group" "kineticadb" {
  name        = "kineticadb-group"
  description = "This machine runs a GPU enhanced database"
  vpc_id      = "${var.vpc_id}"

  # Intracluster access
  ingress {
    from_port   = 1
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["${data.aws_subnet.kineticadb.cidr_block}"]
  }

  # SSH access from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # HTTP access from the VPC
  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
