#!/usr/bin/env bash

function do_apt_update() {
    apt update
    apt install -y python
}

function do_yum_update() {
    yum distro-sync
}
